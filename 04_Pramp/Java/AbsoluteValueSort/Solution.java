import java.io.*;
import java.util.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
Absolute Value Sort
Given an array of integers arr, write a function absSort(arr), that sorts the array according to the absolute values of the numbers in arr. If two numbers have the same absolute value, sort them according to sign, where the negative numbers come before the positive numbers.

Examples:

input:  arr = [2, -7, -2, -2, 0]
output: [0, -2, -2, 2, -7]
Constraints:

[time limit] 5000ms
[input] array.integer arr
0 <= arr.length <= 10
[output] array.integer
 */

class Solution {

  ///==================================================///
  /// Method  2
  /// Time Complexity: O(N^2) from our two for loops.
  /// Space Complexity: O(1), the additional space used by best.
  ///==================================================///
  static boolean smaller(int a, int b){
    if(Math.abs(a) < Math.abs(b)) return true;
    if(Math.abs(a) > Math.abs(b)) return false;
    return a < b;
  }

  static int[] absSort2(int[] arr){
    int best = 0;
    for(int i = 0; i <= arr.length - 2; i++){
      best = i;
      for(int j = i; j <= arr.length - 1; j++){
        if(smaller(arr[j], arr[best])){
          best = j;
        }
      }
      int temp = arr[best];
      arr[best] = arr[i];
      arr[i] = temp;
    }
    return arr;
  }

  ///==================================================///
  /// Method  1
  /// Time Complexity: O(nlogn) from sorting.
  /// Space Complexity: O(N), the additional space used by best.
  ///==================================================///
	static int[] absSort(int[] arr) {
    // your code goes here
    
    /// https://www.itread01.com/content/1548071768.html list <=> int[] <=> Integer
    int[] navArr = Arrays.stream(arr).filter(x -> x <= 0).toArray();
    Integer[] navIntArr = Arrays.stream(navArr).boxed().toArray(Integer[]::new);
    Arrays.sort(navIntArr, Collections.reverseOrder());
    navArr = Arrays.stream(navIntArr).mapToInt(Integer::valueOf).toArray();

    /// 8056ms => 6469ms (delete int[] => Integer[])
    int[] pasArr = Arrays.stream(arr).filter(x -> x > 0).toArray();
    Arrays.sort(pasArr);


    /// Array sort : Dual-Pivot Algorithm => O(nlogn)

    int navIdx = 0;
    int pasIdx = 0;
    int idx = 0;

    int[] output = new int[arr.length];
    
    // System.out.println("========================");
    // System.out.println(Arrays.toString(navArr));
    // System.out.println("========================");
    // System.out.println(Arrays.toString(pasArr));
    // System.out.println("========================");
    
    while(idx < output.length) {
      if(pasIdx == pasArr.length) {
        output[idx] = navArr[navIdx];
        navIdx = navIdx + 1;
        idx = idx + 1;
        continue;
      }

      if(navIdx == navArr.length) {
        output[idx] = pasArr[pasIdx];
        pasIdx = pasIdx + 1;
        idx = idx + 1;
        continue;
      }

      if(Math.abs(navArr[navIdx]) <= pasArr[pasIdx]) {
        output[idx] = navArr[navIdx];
        navIdx = navIdx + 1;
      } else {
        output[idx] = pasArr[pasIdx];
        pasIdx = pasIdx + 1;
      }
      idx = idx + 1;
    }
    return output;
	}
  
	public static void main(String[] args) {
    int[] arr = {2, -7, -2, -2, 0};
        
    int[] answer = absSort2(arr);
    System.out.println(Arrays.toString(answer));
	}
}
