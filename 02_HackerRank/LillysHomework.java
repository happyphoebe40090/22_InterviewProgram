import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class LillysHomework {

    // Complete the lilysHomework function below.
    static int lilysHomework(int[] arr) 
    {
        int size = arr.length;
        
        int minIdx = 0;
        int minNum = 0;
        
        int temp = 0;
    
        boolean findMin = false;
        int swapNum1 = 0;
        
        for(int i = 0; i < size; i++)
        {
            minIdx = i;
            minNum = arr[i];
            findMin = false;
            
            for(int j = i + 1; j < size; j++)
            {
                if(arr[j] < minNum)
                {
                    minIdx = j;
                    minNum = arr[j];
                    findMin = true;
                }
            }
            
            if(findMin)
            {
                temp = arr[i];
                arr[i] = arr[minIdx];
                arr[minIdx] = temp;
                swapNum1 = swapNum1 + 1;
            }
            else
            {
                break;
            }
        }

        int swapNum2 = size - swapNum1;

        if(swapNum1 > swapNum2)
        {
            return swapNum2;
        }
        else
        {
            return swapNum1;
        }
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        // BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        int result = lilysHomework(arr);

        // bufferedWriter.write(String.valueOf(result));
        // bufferedWriter.newLine();

        // bufferedWriter.close();

        scanner.close();
    }
}