#include <iostream>
#include <cstring>
#include <math.h>
#include <stdlib.h> /* 亂數相關函數 */
#include <time.h>   /* 時間相關函數 */
#include <string>
#include <vector>

using namespace std;

class Solution
{
    public:
        bool checkIfExist(vector<int>& arr) {
            for(int i = 0; i < arr.size(); i++) {
                for(int j = 0; j < arr.size(); j++) {
                    if(i == j)
                    {
                        continue;
                    }

                    if(arr[i] * 2 == arr[j] || arr[j] * 2 == arr[i])
                    {
                        printf("m = %d, n = %d\n", arr[i], arr[j]);
                        return true;
                    }
                }
            }
            return false;
        }
};

int main(void)
{
    Solution solution;
    vector<int> arr = {-2,0,10,-19,4,6,-8};
    int answer = solution.checkIfExist(arr);
    printf("answer : %d\n", answer);

    return 0;
}