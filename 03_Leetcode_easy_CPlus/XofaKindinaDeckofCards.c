#include <stdio.h>
#include <stdlib.h>

/// It's wrong.
int hasGroupsSizeX(int* deck, int deckSize){
    int i = 0;
    
    int* groupCount = malloc(10000 * sizeof(int));
    for(i = 0; i < deckSize; i++)
    {
        groupCount[deck[i]] = groupCount[deck[i]] + 1;
        printf("[%d] : %d\n", i, deck[i]);
    }
    
    for(i = 0; i < 10000; i++)
    {
        if((groupCount[i] > 0) && (groupCount[i] % 2 != 0))
        {
            return 0;
        }
    }
    free(groupCount);
    
    return 1;
}

int main(int argc, char* argv[])
{
    int arr[1] = {1};

    int ans = hasGroupsSizeX(arr, 1);
    printf("ans : %d\n", ans);
    return 0;
}