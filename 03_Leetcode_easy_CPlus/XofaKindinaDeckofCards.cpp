#include <iostream>
#include <cstring>
#include <math.h>
#include <stdlib.h> /* 亂數相關函數 */
#include <time.h>   /* 時間相關函數 */
#include <string>
#include <vector>

using namespace std;
/// It's wrong.
class Solution
{
    public:
        bool hasGroupsSizeX(int* deck, int deckSize){
            int i = 0;
            
            int groupCount[10000];
            for(i = 0; i < deckSize; i++)
            {
                groupCount[deck[i]] = groupCount[deck[i]] + 1;
            }
            
            for(i = 0; i < deckSize; i++)
            {
                if((groupCount[i] > 0) && (groupCount[i] % 2 != 0))
                {
                    return false;
                }
            }            
            return true;
        }

        bool hasGroupsSizeX2(vector<int>& deck) {
            return false;
        }   
};

int main(void)
{    
    Solution solution;
    int arr[8] = {1,1,1,2,2,2,3,3};
    bool ans = solution.hasGroupsSizeX(arr, 8);

    printf("ans : %d\n", ans);
}