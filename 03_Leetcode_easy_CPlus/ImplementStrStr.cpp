#include <iostream>
#include <cstring>
#include <math.h>
#include <stdlib.h> /* 亂數相關函數 */
#include <time.h>   /* 時間相關函數 */
#include <string>
#include <vector>

using namespace std;

/// 二維陣列
// int ** array;
// array = new int * [n];     
//         //此矩陣有 n 個列(rows); 先 new 出 n 個 int *
// for (int i=0; i<n; i++)
// {   array[i] = new int [m];
//         //每一列有 m 行(columns); array[i] 指向新 new 出的 m 個 int
// }
 
// // 動態矩陣用完後還給系統, 好習慣! 怎麼 new 來的就怎麼還 !
// for (int i=0; i<n; i++)
// {   delete [] array[i];
// }
// delete [] array; 

class Solution {
    public:
        int strStr(string haystack, string needle) {
            int i = 0;
            int j = 0;
            int ans = -1;
            
            if(needle.length() == 0)
            {
                ans = 0;
                return ans;
            }
            if(needle.length() == 0 && haystack.length() == 0)
            {
                ans = 0;
                return ans;
            }
            if(needle.length() > haystack.length())
            {
                ans = -1;
                return ans;
            }
        
            int **arr = new int* [haystack.length() + 1];
            for(i = 0; i < haystack.length() + 1; i++) {
                arr[i] = new int[needle.length() + 1];
                for(j = 0; j < needle.length() + 1; j++) {
                   arr[i][j] = 0;
                }
            }
            int found = false;
            for(i = 1; i < haystack.length() + 1; i++) {
               for(j = 1; j < needle.length() + 1; j++) {
                   if(haystack[i - 1] == needle[j - 1])
                   {
                       arr[i][j] = arr[i - 1][j - 1] + 1;
                       if(arr[i][j] == needle.length())
                       {
                           found = true;
                           break;
                       }
                   }
               }
               if(found)
               {
                   break;
               }
            }
            
            if(found) {
                for(int k = 0; k <= needle.length(); k++)
                {
                    if(arr[i][j] == 0)
                    {
                        ans = i;
                        break;
                    }

                    i = i - 1;
                    j = j - 1;
                }
            }

            // for(i = 0; i < haystack.length() + 1; i++) {
            //    for(j = 0; j < needle.length() + 1; j++) {
            //        printf("%d ", arr[i][j]);
            //    }
            //    printf("\n");
            // }

            for(i = 0; i < needle.length() + 1; i++) {
                delete[] arr[i];
            }
            delete[] arr;
            
            
            if(needle.length() == 0)
            {
                ans = 0;
            }
            if(needle.length() == 0 && haystack.length() == 0)
            {
                ans = 0;
            }
            if(needle.length() > haystack.length())
            {
                ans = -1;
            }

            return ans;
        }

        int strStr2(string haystack, string needle) {
        if(needle == "") return 0;
        int n1 = haystack.size();
        int n2 = needle.size();
        int i = 0, j = 0;
        while(i < n1 && j < n2)
        {
            if(haystack[i] == needle[j])
            {
                j++;
            }
            else
            {
                i -= j; /// 須注意
                j = 0;
            }  
            i++;
        }
        if(j == n2) return (i - j);
        else return -1;
    }
};


int main(void) {
    string haystack = "baaaa";
    string needle = "aaaa";

    Solution solution;
    int ans = solution.strStr2(haystack, needle);
    printf("Ans : %d\n", ans);

}