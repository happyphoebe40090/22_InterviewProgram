#include <iostream>
#include <cstring>
#include <math.h>
#include <stdlib.h> /* 亂數相關函數 */
#include <time.h>   /* 時間相關函數 */
#include <string>
#include <vector>

class Solution
{
    public:
        int mySqrt(int x) {
            int m = 0;
            int n = 0;

            long long ans = x / 2;
            while((ans * ans) > x){
                if((ans * ans) > x){
                    ans = ans / 2;
                }
                else{
                    ans = ans + 1;
                }
                printf("Step : %d\n", ans);
            }   
            
             while((ans * ans) <= x){
                    ans = ans + 1;
                printf("Step : %d\n", ans);
            }   

            return ans - 1;
        }   
};

int main(void)
{
    Solution solution;
    int ans = solution.mySqrt(100);
    printf("Answer : %d\n", ans);
    return 0;
}