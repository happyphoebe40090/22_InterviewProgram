#include <iostream>
#include <cstring>
#include <math.h>
#include <stdlib.h> /* 亂數相關函數 */
#include <time.h>   /* 時間相關函數 */
#include <string>
#include <vector>

using namespace std; 
class Solution{
    public:
        int firstBadVersion(int n) {
            long long start = 1;
            long long end = n;
            long long mid = n;
            while(start < end)
            {   
                mid = (start + end) / 2;

                if(isBadVersion(mid))
                {
                    end = mid;
                }
                else
                {
                    start = mid + 1;
                }
            }
            return start;
        }
};


int main(void)
{
    Solution solution;
    int n = 1544;
    int answer = solution.firstBadVersion(n);
    printf("answer : %d\n", answer);

    return 0;
}