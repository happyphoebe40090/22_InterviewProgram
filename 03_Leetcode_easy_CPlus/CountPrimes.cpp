#include <iostream>
#include <cstring>
#include <math.h>
#include <stdlib.h> /* 亂數相關函數 */
#include <time.h>   /* 時間相關函數 */

using namespace std; 
class Solution {
    public:
        /// 空間換取時間法則
        int countPrimes(int n) {
            int i = 0;
            int j = 0;
            int res = 0;

            constexpr int LEN = 5000000;
            bool number[LEN] = {false};
            
            for(i = 2; i <= (int)sqrt(n); i++){
                if(number[i] == false){
                    for(j = i * i; j < n; j = j + i){
                        number[j] = true;
                    }
                }
            }
            
            for(i = 2; i < n; i++)
            {
                if(number[i] == false)
                {
                    res = res + 1;
                }
            }

            return res;
        }

        int countPrimes2(int n) {
            int i = 0;
            int j = 0;

            int res = 0;
            bool prinable = true;
            //for(i = 3; i < n; i = i + 2)
            i = 3;
            while(i < n)
            {
                prinable = true;
                
                if((i % 2) == 0)
                {
                    i = i + 1;
                    continue;
                }

                for(j = 3; j < (i / 2); j = j + 2)
                {
                    if((i % j) == 0)
                    {
                        prinable = false;
                        break;
                    }
                }

                if(prinable)
                {
                    printf("Primes number is %d\n", i);
                    res = res + 1;
                }
                else
                {
                
                }
                i = i + 1;
            }

            if(n >= 3)
            {
                printf("Primes number is 2\n");
                res = res + 1; /// for 2
            }

            return res;
        }
};

int main(void){
    int n = 499979;
    int answer = 0;

    Solution s;
    answer = s.countPrimes(n);
    printf("Answer is %d\n", answer);
}