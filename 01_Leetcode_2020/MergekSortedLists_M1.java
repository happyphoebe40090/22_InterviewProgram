/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
 //https://leetcode.com/problems/merge-k-sorted-lists/
class Solution {
    public int checkCnt(ListNode[] lists)
    {
        int cnt = 0;
        int i = 0;
        ListNode node;
        for(i = 0; i < lists.length; i++)
        {
            node = lists[i];
            if (node == null)
            {
                continue;
            }
            while(node.next != null)
            {
                node = node.next;
                cnt ++;
            }
            cnt ++;
        }
        return cnt;
    }
    
    public int[] genElementArray(ListNode[] lists, int cnt)
    {
        int element[] = new int[cnt];
        int eleIndex = 0;
        int i = 0;
        ListNode node;
        
        for(i = 0; i < lists.length; i++)
        {
            node = lists[i];
            if (node == null)
            {
                continue;
            }
            element[eleIndex] = node.val;
            eleIndex ++;
            while(node.next != null)
            {
                node = node.next;
                element[eleIndex] = node.val;
                eleIndex ++;
            }
        }
        return element;
    }
    
    /// wrong sorted
    public int[] bubbleSort(int[] elements)
    {
        int temp = 0;
        int i = 0;
        int j = 0;
        for(i = 0; i < elements.length - 1; i++)
        {
            for(j = i + 1; j < elements.length; j++)
            {
                if(elements[i] > elements[j])
                {
                    temp = elements[i];
                    elements[i] = elements[j];
                    elements[j] = elements[i];
                }
            }
        }
        
        return elements;
    }
    
    public int[] sort(int[] input)
    {
		int length = input.length;//要排列的數字長度
	    int temp;
	    
        //每個數字都要比對
	    for(int i=0;i<length;i++)
        {	
	    	for(int j=0;j<length-1;j++){
	    	   
	    	   //比大小,if 前>後 換位
	    	   if(input[j]>input[j+1]){
	    		   
	    		  temp = input[j];//前面數字先存到變數
	    		  input[j] = input[j+1];//後面數字取代前面數字
	    		  input[j+1] = temp; //將變數(前面數字)存到後面變數
	    		
	    	   }

	    	}
	    	
	    }
		return input;
    }

    public ListNode genListFromArray(int[] elements)
    {
        if(elements.length < 1)
        {
            return null;
        }
        
        ListNode nodeFirst = new ListNode(elements[0]);
        ListNode nodeCur = nodeFirst;
        
        int i = 1;
        for(i = 1; i < elements.length; i++)
        {
            ListNode node = new ListNode(elements[i]);
            nodeCur.next = node;
            nodeCur = nodeCur.next;
        }
        nodeCur.next = null;
        return nodeFirst;
    }
    
    public ListNode mergeKLists(ListNode[] lists) 
    {
        int totalCnt = 0;
        ListNode listAns;
        int[] elementArray; 
        int[] sortedArray; 
        totalCnt = checkCnt(lists);
        elementArray = genElementArray(lists, totalCnt);
        sortedArray = sort(elementArray);
        
        listAns = genListFromArray(sortedArray);
        return listAns;
    }
}