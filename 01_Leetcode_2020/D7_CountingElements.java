class Solution 
{
    public int countElements(int[] arr) 
    {
        int i = 0;
        int j = 0;
        
        int count = 0;
        
        for(i = 0; i < arr.length; i++)
        {
            for(j = 0; j < arr.length; j++)
            {
                if(arr[i] + 1 == arr[j])
                {
                    count++;
                    break;
                }
            }
        }
        return count;
    }
}
