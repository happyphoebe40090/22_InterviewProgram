class Solution 
{
    public int[] productExceptSelf(int[] nums) 
    {
        int resArr[] = new int[nums.length];
        int i = 0;
        int j = 0;
        
        int mutiResult = 1;
        int zeroCnt = 0;
        for(i = 0; i < nums.length; i++)
        {
            if(nums[i] != 0)
            {
                mutiResult = mutiResult * nums[i];
            }
            else
            {
                zeroCnt++;
            }
        }
        
        if(zeroCnt > 1)
        {
            return resArr;
        }
        
        if((nums.length == 2) &&(zeroCnt == 1))
        {
            int temp = 1;
            resArr[0] = resArr[1];
            resArr[1] = temp;
            return resArr;
        }
        
        for(i = 0; i < nums.length; i++)
        {
            if(nums[i] != 0)
            {
                if(zeroCnt == 1)
                {
                    resArr[i] = 0;
                }
                else
                {
                    resArr[i] = mutiResult / nums[i];
                }
            }
            else
            {
              resArr[i] = mutiResult;
            }
        }
        return resArr;
    }
}
