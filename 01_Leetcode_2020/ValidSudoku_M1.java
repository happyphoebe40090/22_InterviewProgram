class Solution 
{
    public boolean isValidSudoku(char[][] board) 
    {
        int i = 0;
        int j = 0;
        
        boolean res = true;
        boolean colRes = true;
        boolean rowRes = true;
        boolean recRes = true;

        for(i = 0; i < board.length; i++)
        {
            for(j = 0; j < board[i].length; j++)
            {
                /// colRes
                for (int col = 0; col < board[i].length; col++)                     
                {
                    if((board[i][j] == board[i][col]) && board[i][j] != '.' && (j!=col))
                    {
                        colRes = false;
                        break;
                    }    
                }
                /// rowRes
                for (int row = 0; row < board.length; row++)                         
                {
                    if((board[i][j] == board[row][j]) && board[i][j] != '.' && (i!=row))
                    {
                        rowRes = false;
                        break;
                    } 
                }
                /// recRes
                int startRecRow = i / 3; 
                int startRecCol = j / 3; 
                for (int row = startRecRow * 3; row < startRecRow * 3 + 3; row++)                             
                {
                    for (int col = startRecCol * 3; col < startRecCol * 3 + 3; col++)                             
                    {
                        if((board[i][j] == board[row][col]) && board[i][j] != '.' &&(j!=col) &&(i!=row))
                        {
                           //recRes = false;
                            break;
                        }
                        
                    }
                    
                }
                
                
            }
            
            if(!colRes || !rowRes || !recRes)
            {
                res = false;
                break;
            }
        }
        return res;
    }
}