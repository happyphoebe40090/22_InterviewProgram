class Solution 
{
    public int myAtoi(String str) 
    {
        long value = 0;
        int signNumber = 0;
        
        int find = 0;
        int idx = -1;
        for(char c : str.toCharArray())
        {           
            idx ++;
            if(c == '-')
            {
                if(find == 1)
                {
                    break;
                }
                if(signNumber > 0)
                {
                    return 0;
                }
                signNumber = 1;
                continue;
            }
            else if(c == '+')
            {
                if(find == 1)
                {
                    break;
                }
                if(signNumber > 0)
                {
                    return 0;
                }
                signNumber = 2;
                continue;
            }
            else if(c == ' ')
            {
                if(find == 1)
                {
                    break;
                }
                if(signNumber > 0)
                {
                    break;
                }
                
                signNumber = 0;
                value = 0;
                continue;
            }
            else if(c < 48 || c > 57)
            {
                if(find == 1)
                {
                    break;
                }
                signNumber = 0;
                value = 0;
                break;
            }
            
            find = 1;
            value = value * 10 + ((int)(c - 48));
            
            if (value > Integer.MAX_VALUE) 
            {
                if (signNumber == 1)
                {
                    return Integer.MIN_VALUE;
                }
                else
                {
                    return Integer.MAX_VALUE;
                }
            }
        }
        
        if(signNumber == 1)
        {
            value = value * (-1);
        }
        
        return (int)value;
    }
}
