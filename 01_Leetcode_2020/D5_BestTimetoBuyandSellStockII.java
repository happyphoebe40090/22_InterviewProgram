class Solution 
{
    public int maxProfit(int[] prices) 
    {
        int index = 0;
        int sum = 0;
        int length = prices.length;
    
        int i = 0;
        boolean finsCh = false;
        while(true)
        {
            if(index >= length)
            {
                break;
            }
            finsCh = false;
            for(i = index; i < length - 1; i++)
            {
                if((prices[i + 1] < prices[i]) && (prices[i] >= prices[index]))
                {
                    sum += (prices[i] - prices[index]);
                    index = i;
                    finsCh = true;
                    break;
                }
            }
            
            if(!finsCh)
            {
                sum += (prices[length - 1] - prices[index]);
                break;
            }
            index++;
            
        }
        return sum;
    }
}
