/**
 * // This is the BinaryMatrix's API interface.
 * // You should not implement it, or speculate about its implementation
 * interface BinaryMatrix {
 *     public int get(int row, int col) {}
 *     public List<Integer> dimensions {}
 * };
 */

class Solution 
{
    public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) 
    {
        List<Integer> dimen = new ArrayList<Integer>();
        dimen = binaryMatrix.dimensions();
        int rowCnt = dimen.get(0);
        int colCnt = dimen.get(1);
        
        int res = -1;
        int i = 0;
        int j = 0;
        
        int oneCnt = 0;
        
        int cmpRowCnt = rowCnt;
        int rowIdx[] = new int[cmpRowCnt];
        for(j = 0; j < rowCnt; j++)
        {
            rowIdx[j] = 1;
            if(binaryMatrix.get(j, colCnt - 1) == 0)
            {
                rowIdx[j] = -1;
            }
        }
        
        int rowHasOne[] = new int[cmpRowCnt];
        for(i = 0; i < colCnt; i++)
        {
            oneCnt = 0;
            for(j = 0; j < rowCnt; j++)
            {
                if(rowIdx[j] == -1)
                {
                    continue;
                }
                
                if(rowHasOne[j] == 1)
                {
                    oneCnt++;
                    continue; 
                }
                
                if(oneCnt > 1)
                {
                    break;
                }
                
                if(binaryMatrix.get(j, i) == 1)
                {
                    rowHasOne[j] = 1;
                    oneCnt++;
                }
                
            }
            
            if(oneCnt >= 1)
            {
                res = i;
                break;
            }
        }
        
        return res;
    }
}
