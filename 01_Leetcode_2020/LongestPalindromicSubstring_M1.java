class Solution 
{
    /// return find the longest length
    private int subPalindrome(String str, int start, int end)
    {
        int len = 0;
        int L = start;
        int R = end;
        
        while(L < R)
        {
            if(str.charAt(L) != str.charAt(R))
            {
                len = 0;
                break;
            }
            len += 2;
            L++;
            R--;
        }
        
        if(L == R)
        {
            if(str.charAt(L) == str.charAt(R))
            {
                len++;   
            }
        }
        return len;
    }
    
    public String longestPalindrome(String s) 
    {
        int longSstart = 0;
        int longSend = 0;
        int longLen = 0;
        
        int strLen = 0;
        if(s != null)
        {
            strLen = s.length();
        }
        
        int startIdx = 0;
        int endIdx = 0;
        int len1 = 0;

        for(startIdx = 0; startIdx < strLen; startIdx++)
        {
            endIdx = strLen - 1; 
            if((endIdx - startIdx) < longLen)
            {
                break;
            }
            
            for(endIdx = strLen - 1; endIdx >= startIdx; endIdx--)
            {
                if((endIdx - startIdx) < longLen)
                {
                    break;
                }
                
                if(longLen == s.length())
                {
                    break;
                }
                    
                len1 = subPalindrome(s, startIdx, endIdx);
                if(len1 > longLen)
                {
                    longLen = len1;
                    longSstart = startIdx;
                    longSend = endIdx;
                    break;
                }
            }
            
            if(longLen == s.length())
            {
                break;
            }
        }
        
        
        if(s != null)
        {
            if(s.length() > 0)
            {
               return s.substring(longSstart, longSend + 1);
            }  
            else
            {
                return "";
            }
        }
        else
        {
           return "";
        }
    }
}
