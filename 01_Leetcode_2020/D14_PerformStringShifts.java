class Solution 
{
    public String stringShift(String s, int[][] shift) 
    {
        int totalShift = 0;
        int dir = 0;
        int i = 0;
        int j = 0;
        
        for(i = 0; i < shift.length; i++)
        {
            if(shift[i][0] == 0)
            {
                totalShift += (-1) * shift[i][1];
            }
            else
            {
                totalShift += (1) * shift[i][1];
            }
        }
        
        if(totalShift < 0)
        {
            dir = -1;
        }
        else
        {
            dir = 1;
        }
        
        String str = new String();
        str = s;
        
        String subStr1 = new String();
        String subStr2 = new String();
        for(i = 0; i < Math.abs(totalShift); i++)
        {
            if(dir < 0)
            {
                subStr1 = str.substring(1,str.length());
                subStr1 = subStr1 + str.substring(0,1);
            }
            else
            {
                subStr1 = str.substring(str.length() - 1,str.length());
                subStr1 = subStr1 + str.substring(0,str.length() - 1);                
            }
            str = subStr1;
        }
        return str;
    }
}
