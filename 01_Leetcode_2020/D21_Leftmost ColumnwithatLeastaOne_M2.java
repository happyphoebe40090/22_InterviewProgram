/* reference : https://qiita.com/vc7/items/7022af54c5e7b0703366*/

class Solution {
    func leftMostColumnWithOne(_ binaryMatrix: BinaryMatrix) -> Int {
        let numberOfRows = binaryMatrix.dimensions()[0]
        let numberOfColumns = binaryMatrix.dimensions()[1]

        var row = 0
        var column = numberOfColumns - 1
        var minFound = -1

        while row < numberOfRows && column >= 0 {
            if binaryMatrix.get(row, column) == 0 {
                row += 1
            } else {
                minFound = column
                column -= 1
            }
        }

        return minFound
    }
}
