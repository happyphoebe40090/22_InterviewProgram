class Solution {
    
    char parentheses[]    = {'(', ')'};
    char squarebrackets[] = {'[', ']'};
    char curlybrackets[]  = {'{', '}'};

    public boolean isValid(String s) {
        boolean bOpenSts = true;
        int len = s.length();
        
        char[] queue = new char[len/2 + 1];
        int queueCnt = 0;
        int index = 0;
        while(true)
        {
            if(index == len)
            {
                break;    
            }
            
            if(s.charAt(index) == parentheses[0] ||
               s.charAt(index) == squarebrackets[0] ||
               s.charAt(index) == curlybrackets[0])
            {
                queue[queueCnt] = s.charAt(index);
                queueCnt ++;
            }
            
            char tar = ' ';
            boolean rightClose = false;
            if(s.charAt(index) == parentheses[1])
            {
                rightClose = true;
                tar = parentheses[0];
            }
            else if(s.charAt(index) == squarebrackets[1])
            {
                rightClose = true;
                tar = squarebrackets[0];
            }
            else if(s.charAt(index) == curlybrackets[1])
            {
                rightClose = true;
                tar = curlybrackets[0];
            }
            
            if(rightClose)
            {
                if(queueCnt > 0)
                {
                    if(queue[queueCnt - 1] == tar)
                    {
                        queueCnt --;
                    }
                    else
                    {
                        bOpenSts = false;
                        break;
                    }
                }
                else
                {
                    bOpenSts = false;
                    break;
                }
            }
            index = index + 1;
        }
        
        if(queueCnt != 0)
        {
            bOpenSts = false;
        }
        
        return bOpenSts;
    }
}
