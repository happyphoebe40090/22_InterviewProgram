class Solution 
{
    /// return find the longest length
    private int subPalindrome(String str, int start, int end)
    {
        int len = 0;
        int L = start;
        int R = end;
        
        while(L >= 0 && R < str.length())
        {
            if(str.charAt(L) != str.charAt(R))
            {
                break;
            }
            L--;
            R++;
        }
        
        if((L == start) && (R == end))
        {
            len = 0;
            
        }
        else
        {
            len =  (R - L) - 1;
        
            if(len > str.length())
            {
                len = str.length();
            }
        }
        return len;
    }
    
    public String longestPalindrome(String s) 
    {
        if (s == null || s.length() < 1) return "";
        int i = 0;
        int len1 = 0;
        int len2 = 0;
        int start = 0, end = 0;
        for(i = 0; i < s.length() - 1; i++)
        {
            len1 = subPalindrome(s, i, i);
            len2 = subPalindrome(s, i, i + 1);
            
            int len = Math.max(len1, len2);
            if (len > end - start) 
            {
                start = i - (len - 1) / 2;
                end = i + len / 2;
            }
        }
        return s.substring(start, end + 1);
    }
}
