class Solution 
{
    public int[] sort(int[] arrs)
    {
        int i = 0;
        int j = 0;
        int temp = 0;
        for(i = 0; i < arrs.length; i++)
        {
            for(j = 0; j < arrs.length - 1; j++)
            {
                if(arrs[j] < arrs[j +1])
                {
                    temp = arrs[j];
                    arrs[j] = arrs[j + 1];
                    arrs[j + 1] = temp; 
                }
            }
        }
        return arrs;
    }
    
    public int lastStoneWeight(int[] stones) 
    {
        int[] sortArr = sort(stones);
        int i = 0;
        int j = 0;
        
        int length = stones.length;
        int index = 0;
        while(length > 1)
        {
            if(sortArr[index] == sortArr[index + 1])
            {
                sortArr[index] = -1;
                sortArr[index + 1] = -1;
                length = length - 2;
            }
            else if(sortArr[index] != sortArr[index + 1])
            {
                sortArr[index] = sortArr[index] - sortArr[index + 1];
                sortArr[index + 1] = -1;
                length--;
            }
            sortArr = sort(sortArr);
        }
        
        if(length < 1)
        {
            return 0;
        }
        else
        {
            return sortArr[0];
        }
    }
}
