#include <iostream>
#include <cstring>
#include <math.h>
#include <stdlib.h> /* 亂數相關函數 */
#include <time.h>   /* 時間相關函數 */
#include <string>
#include <vector>

/// MTK Productoe=r Verify
using namespace std;
bool func(int n)
{
	bool res = true;
    if(n < 2)
    {
        return false;
    }

	while(n > 1)
    {
        if((n % 2) != 0)
        {
            res = false;
            break;
        }
        n = n / 2;
    }
    return res;
}

void toUpper(char *s, int size)
{
	int i = 0;
    for(i = 0; i < size; i++)
    {
        if(s[i] >= 97 && s[i] <= 122)
        {
            s[i] = s[i] - 32;
        }
    }
}

bool isUpper(string s)
{
    bool res = true;

    int length = s.length();
    if(length <= 0)
    {
        return false;
    }

    while(length > 0)
    {
        if(s[length - 1] < 65 || s[length - 1] > 90)
        {
            res = false;
        }

        length = length - 1;
    }

    return res;
}

union aa
{
    unsigned char a;
    short b;
    char *c;
}bb;

int main(void)
{
    // int x = 20;
    // while(x)
    // {
    //     x = x & (x - 1);
    //     printf("%d\n", x);
    // }

    char s[11] = "Q123456789";
    printf("%c \n", (char) *(((long *)s) + 1));
    printf("%d \n", sizeof(union aa ));
    return 0;
}