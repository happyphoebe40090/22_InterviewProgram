import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Third_Maximum_Number
{
    public static int Solution(int[] nums)
    {
        ArrayList<Integer> arrayListInt = new ArrayList<Integer>();
        for(int i = 0; i < nums.length; i++)
        {
            arrayListInt.add(nums[i]);
        }
        
        Collections.sort(arrayListInt);  
        Collections.reverse(arrayListInt);

        int max = arrayListInt.get(0);
        int count = 1;
        
        for(int i = 1; i < nums.length; i++)
        {
            if(max != arrayListInt.get(i))
            {
                count = count + 1;
                max =  arrayListInt.get(i);
            }
            
            if(count >= 3)
            {
                return max;
            }
        }
        return arrayListInt.get(0);
    }

    public static void main(String[] srgs)
    {
        int[] a = {2, 2, 1};
        int res = Solution(a);
        System.out.println(res);
    }
}