import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Excel_Sheet_Column_Title
{
    public static String Solution(int columnNumber)
    {
        String res = "";
        char c = ' ';

        if(columnNumber <= 26)
        {
            c = (char)(64 + columnNumber);
            res = res + String.valueOf(c);
            return res;
        }

        while(columnNumber >= 1)
        {
            if((columnNumber % 26) == 0)
            {
                c = (char)(64 + 26);
                res = res + String.valueOf(c);
                columnNumber = columnNumber - 1;
                //System.out.println(columnNumber);
                columnNumber = columnNumber / 26;
            }
            else
            {
                c = (char)(64 + ((columnNumber % 26)));
                res = res + String.valueOf(c);
                columnNumber = columnNumber / 26;
            }
        }

        StringBuilder input1 = new StringBuilder();
        input1.append(res);
        input1.reverse();
        return input1.toString();
    }

    public static void main(String[] args)
    {
        System.out.println(Solution(Integer.valueOf(args[0])));
    }
}