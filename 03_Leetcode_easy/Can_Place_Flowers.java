public class Can_Place_Flowers
{
  public static boolean Solution(int[] flowerbed, int n)
  {
    boolean res = true;

    int i = 0;
    int zeroCount = 0;

    while(i < flowerbed.length) 
    {
      if(flowerbed[i] == 0) 
      {
        if((i == 0 || flowerbed[i - 1] == 0) && (i == flowerbed.length - 1 || flowerbed[i + 1] == 0))
        {
          flowerbed[i] = 1;
          zeroCount++;
        }
      }
      i++;
    }

    return zeroCount >= n;
  }

  public static void main(String[] args)
  {
    int[] flowerbed = {1, 0, 0, 0, 1};
    int n = 1; 

    System.out.println(Solution(flowerbed, n));
  }
}