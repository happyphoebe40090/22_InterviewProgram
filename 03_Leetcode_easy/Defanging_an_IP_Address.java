import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Defanging_an_IP_Address
{
    public static String Solution(String strIP) 
    {
        String strRes = "";
        strRes = strIP.replace(".", "[.]");
        return strRes;
    }

    public static void main(String[] args)
    {
        String strIP = "255.255.255.255";

        String strRes = Solution(strIP);

        System.out.println(String.format("String IP : %s", strIP));
        System.out.println(String.format("String IP : %s", strRes));
    }
}