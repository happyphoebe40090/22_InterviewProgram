import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
public class Buddy_Strings
{
    public static boolean Solution(String a, String b)
    {
        if(a.length() != b.length())
        {
            return false;
        }
        
        int diffCount = 0;
        char[] diffs1 = new char[2];
        char[] diffs2 = new char[2];

        if(a.equals(b))
        {
            Map<Character, Integer> maps = new HashMap<>();
            for(int i = 0; i < a.length(); i++)
            {
                if(maps.get(a.charAt(i)) != null)
                {
                    return true;
                }
                else
                {
                    maps.put(a.charAt(i),0);
                }
            }
        }
        else
        {
            for(int i = 0; i < a.length(); i++)
            {
                if(a.charAt(i) != b.charAt(i))
                {
                    if(diffCount < 2)
                    {
                        diffs1[diffCount] = a.charAt(i);
                        diffs2[diffCount] = b.charAt(i);
                    }
                    diffCount = diffCount + 1;
                }
                    
                if(diffCount > 2)
                {
                    return false;
                }
            }
        }
        
        if(diffCount == 2)
        {
            if((diffs1[0] == diffs2[1]) && (diffs1[1] == diffs2[0]))
            {
                return true;
            }    
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static void main(String[] args)
    {
        String a = "abcd";
        String b = "badc";

        boolean res = Solution(a, b);

        System.out.println(res);
    }
}