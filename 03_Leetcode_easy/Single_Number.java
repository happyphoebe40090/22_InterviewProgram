import java.util.*;

class Single_Number 
{
  public static int Solution1(int[] nums) 
  {
    HashMap<Integer, Integer> map = new HashMap<>();      
    int res = 0;  
    for(int i = 0; i < nums.length; i++)
    {
      if(map.containsKey(nums[i]))
      {
        map.remove(Integer.valueOf(nums[i]));
      }
      else
      {
        map.put(nums[i], 1);
      }
    }
        
    for (Object key : map.keySet()) 
    {
      res = Integer.parseInt(key.toString());
    }
        
    return res;
  }

  public static int Solution2(int[] nums) 
  {
    HashMap<Integer, Integer> map = new HashMap<>();      
    int res = 0;  
    for(int i = 0; i < nums.length; i++)
    {
      map.put(nums[i], map.getOrDefault(nums[i], 0) + 1); 
    }

    for(int i : nums) 
    {
      if (map.get(i) == 1) 
      {
        return i;
      }
    }
    return 0;
  }

  public static int Solution3(int[] nums) 
  {
    int a = 0;
    for (int i : nums) 
    {
      a ^= i;  /// XOR 運算
    }
    return a;
  }

  public static int Solution4(int[] nums) 
  {
    int sumOfSet = 0, sumOfNums = 0;
    Set<Integer> set = new HashSet<Integer>();

    for (int num : nums) {
      if (!set.contains(num)) {
        set.add(num);
        sumOfSet += num;
      }
      sumOfNums += num;
    }
    return 2 * sumOfSet - sumOfNums;
  }

  public static void main(String[] args)
  {
    int[] input = {2, 1, 1, 2, 5};
    int res = 0;

    res = Solution1(input);
    
    System.out.println(String.format("Res : %d", res));
  }
}