import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Running_Sum_of_1d_Array
{
    /// Solution 1 used extra memory to store result
    public static int[] Solution1(int[] nums) 
    {
        int[] resArr = new int[nums.length];
        
        resArr[0] = nums[0];

        for(int i = 1; i < resArr.length; i++)
        {
            resArr[i] = nums[i] + resArr[i - 1];
            nums[i] = nums[i] + resArr[i - 1];
        }

        return resArr;
    }

    /// Solution 2 use the original arr to store result
    public int[] Solution2(int[] nums) 
    {
        for(int i = 1; i < nums.length; i++)
        {
            nums[i] = nums[i] + nums[i - 1];
        }
        return nums;
    }

    public static void main(String[] args)
    {
        /// test case 
        int[] testArr = new int[4];
        testArr[0] = 1;
        testArr[1] = 1;
        testArr[2] = 1;
        testArr[3] = 1;

        int[] resArr = Solution(testArr);

        for(int i = 0; i < testArr.length; i++)
        {
            System.out.println(String.format("testArr [%d] = %d", i, testArr[i]));
        } 

        for(int i = 0; i < resArr.length; i++)
        {
            System.out.println(String.format("resArr [%d] = %d", i, resArr[i]));
        } 
    }
}