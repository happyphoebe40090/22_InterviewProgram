
public class Reverse_Integer
{
    public static int Solution(int x)
    {
        int res = 0;
        int pop = 0;
        boolean reverse = false;

        if(x < 0)
        {
            x = x * -1;
            reverse = true;
        }

        while(x > 0)
        {
            pop = x % 10;
            x = x / 10;
            if (res > Integer.MAX_VALUE/10 || (res == Integer.MAX_VALUE / 10 && pop > 7)) return 0;
            if (res < Integer.MIN_VALUE/10 || (res == Integer.MIN_VALUE / 10 && pop < -8)) return 0;    
            res = res * 10;
            res = res + pop;
        }

        if(reverse)
        {
            res = res * -1;
        }
        return res;
    }

    public static void main(String[] args)
    {
        int test = -2143847412;
        int res = Solution(test);
        System.out.println(String.format("Reverse : %d", res));
    }
}