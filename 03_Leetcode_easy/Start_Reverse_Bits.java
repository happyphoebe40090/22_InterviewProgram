
public class Start_Reverse_Bits
{
    public static int Solution(int n)
    {
        int ans = 0;
        //int pop = 0;

        for (int i = 0; i < 32; i++) 
        {
            ans = ans << 1;
            //pop = n & 1; /// Wrong  wrong : pop = n % 2;
            ans = ans + (n & 1); /// ans = ans | pop;
            n = n >> 1;
        }
        return ans;
    }

    public static void main(String[] args)
    {
       int res = 43261596;
       res = Solution(res);
       System.out.println(String.format("Res : %d", res));

       res = 43261596;
       System.out.println(String.format("Res : %d", res & 1));
       System.out.println(String.format("Res : %d", res % 2));
    }
}